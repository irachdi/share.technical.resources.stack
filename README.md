# Getting started with the docker stacks

## A. Installation

You need Git and Docker on your machine.

[Go to the Docker CE download page](https://www.docker.com/community-edition#/download)

For Windows, you can  install Docker Toolbox on Windows (https://docs.docker.com/toolbox/toolbox_install_windows). After Installation completed, Use the Shortcut "Docker Quickstart Terminal" on Desktop.

### A.1. Ensure recent Git, Docker and Docker Compose are installed

``` bash
git version
```
> git version 2.16.2.windows.1

``` bash
docker -v
```
> Docker version 18.03.0-ce, build 0520e24302

``` bash
docker-compose -v
```
> docker-compose version 1.20.1, build 5d8c71b2

### A.2. Pull the git repositories - Devops part (that contains docker-stack)

``` bash
cd ~/git
git clone git@gitlab.com:irachdi/share.technical.resources.stack.git

```

## B. Lunch the stack

Here are the available stacks :

- local


### B.1. Go to a local stack

``` bash
cd ~/share.technical.resources.stack/stacks/local
```


### B.2. Create the stack (this command is also used to update the stack)

``` bash
docker-compose up -d
```
``` bash
Creating network "local1_default" with the default driver
Creating local1_mongo_1 ... done

### B.3. Inspect the services

Inspect mongo
``` bash
docker-compose logs -f mongo [ctrl+c]
docker-compose exec mongo mongo
 > use test
 > show tables
 > exit
```

## C. Create a new service

> Create a new yml service in SERVICES directory

> Add its path to COMPOSE_FILE variable in .env file located in : STACKS/local or STACKS/prod for example